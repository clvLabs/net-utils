#!env python3
# coding=utf-8

# Based on https://stackoverflow.com/questions/26174743/python-making-a-fast-port-scanner
import argparse
import sys
import socket
import threading
import queue
import time
import json
from src.config import cfg
import src.ui as ui

# ------------------------------------------------------------------
# Make it easier (shorter) to use cfg object
psCfg = cfg['port-scanner']

PORT_WIDTH = 5
NUM_THREADS = 30

def TCP_connect(host, port_number, timeout):
  TCPsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  TCPsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
  TCPsock.settimeout(timeout)
  TCPsock.connect((host, port_number))

def threadProc(threadId, host, q, timeout, output):
  while True:
    try:
      port = q.get_nowait()
    except:
      break

    try:
      print('{0}:{1}\r'.format(host, port), end='')
      TCP_connect(host, port, timeout)
      output[port] = 'Listening'
    except:
      output[port] = ''

    q.task_done()

def scan_ports(wnpl, host, all, minPort, maxPort, timeout, threadDelay):
  ui.printLine('')
  ui.printLine('Scanning...', 'TITLE')

  threads = []    # To run TCP_connect concurrently
  output = {}     # For printing purposes

  # Prepare job queue
  q = queue.Queue()
  if all or not wnpl:
    for p in range(minPort, maxPort+1):
      q.put(p)
  else:
    for k in wnpl:
      q.put(int(k))

  # Spawning threads to scan ports
  for i in range(NUM_THREADS):
    t = threading.Thread(target=threadProc, args=(i, host, q, timeout, output))
    threads.append(t)

  # Starting threads
  for t in threads:
    t.start()
    time.sleep(threadDelay)

  # Locking the script until all threads complete
  for t in threads:
    t.join()

  ui.clearLine()

  # Printing listening ports from small to large
  hits = 0
  for p in output:
    if output[p] == 'Listening':
      hits += 1
      listOpenPort(host, p, wnpl)

  ui.printLine('')
  ui.printLine('{0} ports open in range'.format(hits),
    'SOMEFOUND' if hits else 'NONEFOUND')

def listOpenPort(host, port, wnpl):
  key = str(port)
  if key in wnpl:
    wnp = wnpl[key]

    if type(wnp) is not list:
      wnp = [wnp]

    for p in wnp:
      if p['tcp']:
        ui.printLine('{} {} {} ({})'.format(
          host,
          str(port).ljust(PORT_WIDTH),
          p['description'],
          p['status'],
          ), 'FOUND')
  else:
        ui.printLine('{} {} {} ({})'.format(
          host,
          str(port).ljust(PORT_WIDTH),
          'UNKNOWN',
          '',
          ), 'FOUND')

# ---[ Main code section ]--------------------------------------------

def main():
  ui.setCurrentModule('port-scanner')
  ui.showModuleTitle()

  parser = argparse.ArgumentParser()
  parser.add_argument('host', nargs='?', help='host to scan (default: {0})'.format(psCfg['host']), default=psCfg['host'])
  parser.add_argument('-a', '--all', action='store_true', help='scan all ports? (otherwise scans only well-known ports)')
  parser.add_argument('-m', '--minport', type=int, help='min port to scan (default: {0})'.format(psCfg['minPort']), default=psCfg['minPort'])
  parser.add_argument('-x', '--maxport', type=int, help='max port to scan (default: {0})'.format(psCfg['maxPort']), default=psCfg['maxPort'])
  parser.add_argument('-t', '--timeout', type=float, help='TCP connection timeout (default: {0})'.format(psCfg['timeout']), default=psCfg['timeout'])
  parser.add_argument('-d', '--threaddelay', type=float, help='delay between threads (default: {0})'.format(psCfg['threadDelay']), default=psCfg['threadDelay'])
  args = parser.parse_args()

  # Try to get "well known port list"
  ui.printLine('Trying to read well-known port list')
  try:
    with open('ports.json') as jsonPorts:
      wnpl = json.loads(jsonPorts.read())['ports']
    ui.printLine('Well-known ports file loaded', 'MSG')
  except:
    wnpl = {}
  ui.printLine('')

  # Show current configuration
  ui.printLine('Host:         {0}'.format(args.host))
  ui.printLine('Scan range:   {0}'.format('ALL ports in range' if args.all else 'well-known ports'))
  if args.all or not wnpl:
    ui.printLine('Min port:     {0}'.format(args.minport))
    ui.printLine('Max port:     {0}'.format(args.maxport))
  ui.printLine('TCP timeout:  {0}'.format(args.timeout))
  ui.printLine('Thread delay: {0}'.format(args.threaddelay))

  # Scan!
  scan_ports(
    wnpl,
    args.host,
    args.all,
    args.minport,
    args.maxport,
    args.timeout,
    args.threaddelay)

if __name__ == '__main__':
  try:
    main()
  except SystemExit:
    pass
  except:
    print()
    print()
    print('----------------------------------------------')
    print('An error happened, execution interrupted:')
    print(sys.exc_info())
