#!env python3
# coding=utf-8
import subprocess
import sys
import socket
import threading

from src.config import cfg
import src.ui as ui
import src.net as net

# ------------------------------------------------------------------
# Make it easier (shorter) to use cfg object
lanCfg = cfg['lan']
nsCfg = cfg['network-scanner']

NAME_WIDTH = nsCfg['width']['name']
MAC_WIDTH = nsCfg['width']['mac']
TYPE_WIDTH = nsCfg['width']['type']
IP_WIDTH = nsCfg['width']['ip']
KNOWN_WIDTH = nsCfg['width']['known']

# ------------------------------------------------------------------
# Ping a single host
def pingHost(ip, ARPCache, output):
  addr = lanCfg['baseIP'] + str(ip)

  ui.printLine('{0}: waiting'.format(addr), 'PING_WAITING', lf=False)

  res = net.ping(addr)
  arp = net.getARPEntryByIP(ARPCache, addr)

  if res['returnCode'] == 0:
    color = 'PING_OK' if arp and arp['known'] else 'PING_OK?'
    ui.printLine('{0}: ping OK'.format(addr), color, lf=False)
  elif res['returnCode'] == 2:
    color = 'PING_NORES' if arp and arp['known'] else 'PING_NORES?'
    ui.printLine('{0}: NO RESPONSE'.format(addr), color, lf=False)
  else:
    color = 'PING_FAILED' if arp and arp['known'] else 'PING_FAILED?'
    ui.printLine('{0}: FAILED'.format(addr), color, lf=False)

  output[ip] = {
    'addr': addr,
    'res': res['returnCode'],
    'color': color
  }


# ------------------------------------------------------------------
# Ping full range of IPs
def pingHostRange(output, ARPCache):
  threads = []    # To run TCP_connect concurrently

  for ip in range(nsCfg['minIP'], nsCfg['maxIP']):
    t = threading.Thread(target=pingHost, args=(ip, ARPCache, output))
    threads.append(t)

  for t in threads:
    t.start()

  # Lock the script until all threads complete
  for t in threads:
    t.join()


# ------------------------------------------------------------------
# Show range ping results
def showPingResult(output):
  okCount = 0
  line = ''

  for hostIndex in output:
    host = output[hostIndex]
    hostStr = '{0} '.format(str(hostIndex).rjust(3))
    hostStr = ui.setStrColor(hostStr, host['color'])
    line += hostStr
    lineFeed = (hostIndex % nsCfg['hostsPerLine']) == 0

    if lineFeed:
      ui.printLine(line)
      line = ''

    if host['res'] == 0:
      okCount += 1

  # Print last line
  ui.printLine(line)
  ui.printLine('')

  ui.printLine('{0} responses OK'.format(okCount))
  ui.printLine('')


# ------------------------------------------------------------------
# Show the ARP cache
def showARPCache(cache):
  headerStr = '{0} {1} {2} {3} {4} '.format(
    'IP'.ljust(IP_WIDTH),
    'NAME'.ljust(NAME_WIDTH),
    'MAC'.ljust(MAC_WIDTH),
    'TYPE'.ljust(TYPE_WIDTH),
    'KNOWN'.ljust(KNOWN_WIDTH),
    )
  ui.printLine(headerStr, 'HEADER')

  for item in cache:
    ARPLine = '{0} {1} {2} {3} {4} '.format(
      item['ip'][:IP_WIDTH].ljust(IP_WIDTH),
      item['name'][:NAME_WIDTH].ljust(NAME_WIDTH),
      item['mac'][:MAC_WIDTH].ljust(MAC_WIDTH),
      item['type'][:TYPE_WIDTH].ljust(TYPE_WIDTH),
      ('YES' if item['known'] else 'NO')[:KNOWN_WIDTH].ljust(KNOWN_WIDTH),
      )

    if item['type'] == net.ARPTYPE_LOCALHOST:
      color = 'ARP_LOCAL'
    elif item['type'] == net.ARPTYPE_KNOWN:
      # color = 'ARP_KNOWN'
      continue # Skip line !
    else:
      color = 'ARP_DYN' if item['known'] else 'ARP_DYN?'

    ui.printLine(ARPLine, color)
  ui.printLine()

# ---[ Main code section ]--------------------------------------------
def main():
  ui.setCurrentModule('network-scanner')
  ui.showModuleTitle()

  # First read of ARP cache
  ui.printLine('Reading ARP cache...', 'TITLE')
  ui.printLine()
  ARPCache = net.readARPCache()

  # Ping full range of IPs
  ui.printLine('Scanning full range of IPs in {0}...'.format(lanCfg['baseIP']), 'TITLE')
  ui.printLine()

  output = {}
  pingHostRange(output, ARPCache)

  output = {k:output[k] for k in sorted(output.keys())}

  showPingResult(output)

  # Read/show ARP cache
  ui.printLine('Re-reading ARP cache...', 'TITLE')
  ui.printLine()
  ARPCache = net.readARPCache()
  showARPCache(ARPCache)

if __name__ == '__main__':
  try:
    main()
  except:
    print()
    print()
    print('----------------------------------------------')
    print('An error happened, execution interrupted:')
    print(sys.exc_info())
