# ------------------------------------------------------------------
# net-utils configuration loader
#
# Loads user config if present, default otherwise
#

try:
  from src.config_user import cfg as user
  cfg = user
except:
  from src.config_default import cfg as default
  cfg = default

# ---[ Main code section ]-----------------------------------------------------------------------------------

if __name__ == "__main__":
  print('This module is a library, it should not be executed directly')
