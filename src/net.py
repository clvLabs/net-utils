# ------------------------------------------------------------------
# net-utils network module
#

import re
import os
import subprocess

import src.utils as ut
from src.config import cfg

# ------------------------------------------------------------------
# Make it easier (shorter) to use cfg object
lanCfg = cfg['lan']


ARPTYPE_LOCALHOST = 'LOCALHOST'
ARPTYPE_KNOWN = 'KNOWN'


def ping(hostName, pingCount=1, pingMaxWait=1):
  if ut.isWindows():
    pingCountParam = '-n'
    pingMaxWaitParam = '-w'
  else:
    pingCountParam = '-c'
    pingMaxWaitParam = '-W'

  proc = subprocess.Popen([
    'ping',
    pingCountParam, str(pingCount),
    pingMaxWaitParam, str(pingMaxWait),
    hostName]
    ,stdout=subprocess.PIPE)

  stdout, stderr = proc.communicate()

  return {
    'returnCode': proc.returncode,
    'stdout': stdout.decode('ASCII'),
  }


def translateMAC(mac):
  return mac.upper().replace('-', ':')


def getARPEntryByMAC(cache, mac):
  for entry in cache:
    if entry['mac'] == mac:
      return entry
  return None


def getARPEntryByIP(cache, ip):
  for entry in cache:
    if entry['ip'] == ip:
      return entry
  return None


def getHostMAC(name):
  for host in lanCfg['knownHosts']:
    if host[1] == name:
      return host[0]
  return None


def getHostName(mac):
  for host in lanCfg['knownHosts']:
    if host[0] == mac:
      return host[1]
  return ''


def getHostIsKnown(mac):
  for host in lanCfg['knownHosts']:
    if host[0] == mac:
      return True
  return False


def getLocalHostConfig():
  if ut.isWindows():
    return getLocalHostConfig_Windows()
  else:
    return getLocalHostConfig_POSIX()

def getLocalHostConfig_Windows():
  proc = subprocess.Popen(['ipconfig', '/all'],stdout=subprocess.PIPE)
  stdout, stderr = proc.communicate()

  ip = ''
  mac = ''

  for line in stdout.decode('ASCII').split('\n'):
    line = line.replace('\r', '')

    if 'Physical Address' in line:
      line = line.replace('Physical Address', '')
      line = line.replace('. ', '')
      line = line.replace(':', '')
      line = line.replace(' ', '')
      mac = translateMAC(line)

    if 'IPv4 Address' in line and lanCfg['baseIP'] in line:
      line = line.replace('IPv4 Address', '')
      line = line.replace('(Preferred)', '')
      line = line.replace('. ', '')
      line = line.replace(':', '')
      line = line.replace(' ', '')
      ip = line

      return {
        'mac': mac,
        'ip': ip,
      }

  # Not found (????)
  return {
    'mac': '',
    'ip': '',
  }

def getLocalHostConfig_POSIX():
  proc = subprocess.Popen(['ip', 'a'],stdout=subprocess.PIPE)
  stdout, stderr = proc.communicate()

  ip = ''
  mac = ''

  for line in stdout.decode('ASCII').split('\n'):
    line = line.replace('\r', '')

    matchObj = re.match( r'(.*) link/ether (([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2}))', line, re.M|re.I)

    if matchObj:
      mac = translateMAC(matchObj.group(2))

    matchObj = re.match( r'(.*)inet (\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b)', line, re.M|re.I)
    if matchObj and lanCfg['baseIP'] in matchObj.group(2):
      ip = matchObj.group(2)

      return {
        'mac': mac,
        'ip': ip,
      }

  # Not found (????)
  return {
    'mac': '',
    'ip': '',
  }


def readARPCache():
  cache = []

  # Start list with local host
  localCfg = getLocalHostConfig()

  cache.append({
    'mac': localCfg['mac'],
    'ip': localCfg['ip'],
    'type': ARPTYPE_LOCALHOST,
    'name': getHostName(localCfg['mac']),
    'known': getHostIsKnown(localCfg['mac']),
  })

  # Call ARP system command and get output
  proc = subprocess.Popen(['arp', '-a'],stdout=subprocess.PIPE)
  stdout, stderr = proc.communicate()

  # Take note of the broadcast IP to filter it
  broadcastAddr = lanCfg['baseIP'] + '255'

  # Read ARP cache
  for entry in stdout.decode('ASCII').split('\n'):
    fields = entry.split()
    if len(fields) > 0:
      if ut.isWindows():
        _ip = fields[0]
        _mac = fields[1]
        _type = fields[2]
      else:
        _ip = fields[1].replace('(','').replace(')','')
        _mac = fields[3].upper()
        _type = fields[4]
        # Skip incomplete ARP entries!
        if 'INCOMPLETE' in _mac:
          continue

      foundIP = _ip
      if lanCfg['baseIP'] in foundIP and foundIP != broadcastAddr:
        mac = translateMAC(_mac)

        newItem = {
          'mac': mac,
          'ip': _ip,
          'type': _type,
          'name': getHostName(mac),
          'known': getHostIsKnown(mac),
        }
        cache.append(newItem)

  # Add missing hosts from config file
  for host in lanCfg['knownHosts']:
    found = False
    for entry in cache:
      if host[0] == entry['mac']:
        found = True

    if not found:
      cache.append({
        'mac': host[0],
        'ip': '',
        'type': ARPTYPE_KNOWN,
        'name': host[1],
        'known': True,
      })

  return cache



# ---[ Main code section ]-----------------------------------------------------------------------------------

if __name__ == "__main__":
  print('This module is a library, it should not be executed directly')
