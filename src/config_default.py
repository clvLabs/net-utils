# ------------------------------------------------------------------
# net-utils default configuration
#
# PLEASE DO NOT CHANGE CONFIG_DEFAULT.PY
#  UNLESS YOU KNOW WHAT YOU'RE DOING
#
# To change your personal settings please make a copy of this file,
#  rename as config_user.py and edit as needed.
#

# Support variables (will remain local to this module)
MAXPING_ETHERNET = 3
MAXPING_WIFI = 50
MAXPING_SMARTPHONE = 200

# Configuration object
cfg = {

  # ---[LAN configuration (global)]--------------------------------------
  'lan': {
    'baseIP': '192.168.1.',
    'knownHosts': [
      [ '11:11:11:11:11:11', 'Home router' ],
      [ '22:22:22:22:22:22', 'Main PC' ],
      [ '33:33:33:33:33:33', 'Smartphone 1' ],
      [ '44:44:44:44:44:44', 'Smartphone 2' ],
      [ '55:55:55:55:55:55', 'Tablet' ],
    ],
  },

  # ---[Interface configuration (global)]--------------------------------------
  'ui': {
    'maxLineLen': 80,
    'color': {
      '':         'white',    # Default
      'TITLE':    'white+',
      'HEADER':   'cyan+',
      'MSG':      'yellow+',
      'WAITING':  'cyan+',
    },
  },

  # ---[ping-monitor configuration]--------------------------------------
  'ping-monitor': {
    'defaultMaxPing': 20,
    'avgPingTimeMaxSamples': 100,

    'hosts': [
      [ '', 'Home router', MAXPING_ETHERNET ],
      [ '', 'Main PC', MAXPING_ETHERNET ],
      [],
      [ '', 'Smartphone 1', MAXPING_SMARTPHONE ],
      [ '', 'Smartphone 2', MAXPING_SMARTPHONE ],
      [ '', 'Tablet', MAXPING_SMARTPHONE ],
      [],
      [ '8.8.8.8',    'Google DNS', 15 ],
      [ 'google.com', 'Google' ],
    ],

    'color': {
      'WAIT':     'black+',
      'KNOWN':    'black+',
      'AVG':      'yellow+',
      'TOUT':     'red+',
      'SLOW':     'magenta+',
      'DOWN':     'red+',
      'UP':       'green+',
    },

    # Column widths
    'width': {
      'hostName': 20,
      'hostIp': 15,
      'hostAlias': 15,
      'lastping': 6,
      'avgping': 6,
      'maxping': 3,
      'state': 6,
      'pct': 6,
    },
  },

  # ---[network-scanner configuration]--------------------------------------
  'network-scanner': {
    'minIP': 1,
    'maxIP': 255,
    'hostsPerLine': 29,

    'color': {
      'PING_WAITING':  'cyan+',
      'PING_OK':       'green+',
      'PING_OK?':      'yellow+',
      'PING_NORES':    'red',
      'PING_NORES?':   'red+',
      'PING_FAILED':   'red',
      'PING_FAILED?':  'black+',
      'ARP_LOCAL':     'green+',
      'ARP_DYN':       'green',
      'ARP_DYN?':      'yellow+',
      'ARP_KNOWN':     'black+',
    },

    # Column widths
    'width': {
      'name': 30,
      'mac': 20,
      'type': 10,
      'ip': 15,
      'known': 5,
    },
  },

  # ---[port-scanner configuration]--------------------------------------
  'port-scanner': {
    'host': 'localhost',
    'minPort': 1,
    'maxPort': 1000,
    'timeout': 0.5,
    'threadDelay': 0.05,
    'color': {
      'FOUND':     'green',
      'SOMEFOUND': 'green+',
      'NONEFOUND': 'red+',
    },
  },
}
