# ------------------------------------------------------------------
# net-utils ui module
#

import sys
import os

import src.utils as ut
from src.config import cfg

gCurrentModule = ''

# Clear the whole screen
def cls():
  if ut.isWindows():
    os.system('cls')
  else:
    os.system('clear')


# Clear the current text line
def clearLine():
    print('{:s}'.format(' ' * cfg['ui']['maxLineLen']), end='\r')


# Give a string some ANSI color formatting
def colorStr(string, foreColor, backColor='black'):
  attr = []
  if foreColor.find('+') != -1:
    foreColor = foreColor.replace('+','')
    attr.append('1')  # bright

  if foreColor == 'black':  attr.append('30')
  elif foreColor == 'red':  attr.append('31')
  elif foreColor == 'green':  attr.append('32')
  elif foreColor == 'yellow': attr.append('33')
  elif foreColor == 'blue': attr.append('34')
  elif foreColor == 'magenta':  attr.append('35')
  elif foreColor == 'cyan': attr.append('36')
  elif foreColor == 'white':  attr.append('37')

  if backColor == 'black':  pass
  elif backColor == 'red':  attr.append('41')
  elif backColor == 'green':  attr.append('42')
  elif backColor == 'yellow': attr.append('43')
  elif backColor == 'blue': attr.append('44')
  elif backColor == 'magenta':  attr.append('45')
  elif backColor == 'cyan': attr.append('46')
  elif backColor == 'white':  attr.append('47')

  return '\x1b[%sm%s\x1b[0m' % (';'.join(attr), string)

def setStrColor(str, foreColor='white', backColor='black'):
  if foreColor in cfg[gCurrentModule]['color']:
    foreColor = cfg[gCurrentModule]['color'][foreColor]
  elif foreColor in cfg['ui']['color']:
    foreColor = cfg['ui']['color'][foreColor]

  if "/" in foreColor:
    parts = foreColor.split("/")
    foreColor = parts[0]
    backColor = parts[1]

  if backColor in cfg[gCurrentModule]['color']:
    backColor = cfg[gCurrentModule]['color'][backColor]
  elif backColor in cfg['ui']['color']:
    backColor = cfg['ui']['color'][backColor]

  return colorStr(str, foreColor, backColor)

def printLine(str='', foreColor='', lf=True):
  if lf:
    print(setStrColor(str.ljust(50), foreColor))
  else:
    print(setStrColor(str.ljust(50), foreColor), end='\r')
    sys.stdout.flush()

def setCurrentModule(module):
  global gCurrentModule
  gCurrentModule = module

def showModuleTitle():
  printLine(gCurrentModule, 'TITLE')
  printLine('=' * len(gCurrentModule), 'TITLE')


# ---[ Main code section ]-----------------------------------------------------------------------------------

if __name__ == "__main__":
  print('This module is a library, it should not be executed directly')
