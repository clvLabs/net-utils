# ------------------------------------------------------------------
# net-utils utility module
#
import os

def isWindows():
  return (os.name == 'nt')


# ---[ Main code section ]-----------------------------------------------------------------------------------

if __name__ == "__main__":
  print('This module is a library, it should not be executed directly')