#net-utils#

Python utility package for network monitoring

##Installation##

* Make sure you have [python 3](https://www.python.org/downloads/) installed
* Clone this repo or download and extract the .zip file
* Get the *well known port list* (run at project folder)
  * ```wget https://raw.githubusercontent.com/mephux/ports.json/master/ports.json```

##Configuration##

Go to `src` folder

Copy `config_default.py` as `config_user.py`

Edit `config_user.py`

**NOTES**

`MAXPING_ETHERNET` / `MAXPING_WIFI` / `MAXPING_SMARTPHONE` are set as examples on using variables on configuration files. Feel free to add as many as you need.

`cfg['lan']['knownHosts']` should contain a list of your network devices' MAC addresses together with a device description.

```
    'knownHosts': [
      [ '11:11:11:11:11:11', 'Home router' ],
      [ '22:22:22:22:22:22', 'Main PC' ],
      [ '33:33:33:33:33:33', 'Smartphone 1' ],
      [ '44:44:44:44:44:44', 'Smartphone 2' ],
      [ '55:55:55:55:55:55', 'Tablet' ],
    ],
```

`cfg['ping-monitor']['hosts']` can contain:

* Known hosts by MAC: by leaving the first field empty and using the device description set in `cfg['lan']['knownHosts']`. This allows to recognize devices even if DHCP server changes their IP address.

* Known hosts by IP/hostName: by using the first field to indicate the `ping` target and using the second field as a description.

* Separator lines: by adding empty items (`[],`), hosts can be visually grouped.

```
    'hosts': [
      [ '', 'Home router', MAXPING_ETHERNET ],
      [ '', 'Main PC', MAXPING_ETHERNET ],
      [],
      [ '', 'Smartphone 1', MAXPING_SMARTPHONE ],
      [ '', 'Smartphone 2', MAXPING_SMARTPHONE ],
      [ '', 'Tablet', MAXPING_SMARTPHONE ],
      [],
      [ '8.8.8.8',    'Google DNS', 15 ],
      [ 'google.com', 'Google' ],
    ],
```

##Usage##

###Ping monitor###
`python3 ping-monitor.py`

No parameters are needed

###Network scanner###
`python3 network-scanner.py`

No parameters are needed

###Port scanner###
```
python3 port-scanner.py [-h] [-a] [-m MINPORT] [-x MAXPORT] [-t TIMEOUT]
                    [-d THREADDELAY]
                    [host]

positional arguments:
  host                  host to scan (default: localhost)

optional arguments:
  -h, --help            show this help message and exit
  -a, --all             scan all ports? (otherwise scans only well-known
                        ports)
  -m MINPORT, --minport MINPORT
                        min port to scan (default: 1)
  -x MAXPORT, --maxport MAXPORT
                        max port to scan (default: 1000)
  -t TIMEOUT, --timeout TIMEOUT
                        TCP connection timeout (default: 0.5)
  -d THREADDELAY, --threaddelay THREADDELAY
                        delay between threads (default: 0.05)
```
