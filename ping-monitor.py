#!env python3
# coding=utf-8
import sys
import subprocess
import re
import os
from functools import reduce
from time import sleep

from src.config import cfg
import src.ui as ui
import src.utils as ut
import src.net as net

# ------------------------------------------------------------------
# Make it easier (shorter) to use cfg object
pmCfg = cfg['ping-monitor']

HOST_NAME_WIDTH = pmCfg['width']['hostName']
HOST_IP_WIDTH = pmCfg['width']['hostIp']
HOST_ALIAS_WIDTH = pmCfg['width']['hostAlias']
LASTPING_WIDTH = pmCfg['width']['lastping']
AVGPING_WIDTH = pmCfg['width']['avgping']
MAXPING_WIDTH = pmCfg['width']['maxping']
STATE_WIDTH = pmCfg['width']['state']
PCT_WIDTH = pmCfg['width']['pct']

# ------------------------------------------------------------------
# Configuracio interna

# Elements la llista de hosts
HOST_NAME = 0     # IP / nom DNS del host
HOST_ALIAS = 1    # Alias del host (opcional)
HOST_MAXPING = 2  # Temps maxim de ping pel host (opcional)
HOST_STATS = 3    # Estadistiques del host (INTERN)

# ------------------------------------------------------------------
# Funcions

# Omplir la informacio que falti a la llista de hosts
def fillConfig(ARPCache):
  hosts = pmCfg['hosts']

  # Keep track of ARP "unknown" hosts
  for arp in ARPCache:
    if not arp['known']:
      hosts.insert(0, [arp['ip'], 'UNKNOWN'])

  for host in hosts:
    emptyLine = False

    if( len(host) <= HOST_NAME ):
      emptyLine = True
      host.append('')

    if( len(host) <= HOST_ALIAS ):
      host.append('')

    if( len(host) <= HOST_MAXPING ):
      host.append(pmCfg['defaultMaxPing'])

    host.append({
      'emptyLine': emptyLine,
      'IP': '???',
      'pingTimes': [],
      'avgPingTime': 0.0,
      'totalCount': 0,
      'okCount': 0,
      'timeoutCount': 0,
      'errorCount': 0,
    })

# Fer una 'ronda' de pings i 'prendre nota' dels resultats
def pingAllHosts(ARPCache):
  hosts = pmCfg['hosts']

  for host in hosts:
    if not host[HOST_STATS]['emptyLine']:   # ignore empty lines
      hostName = host[HOST_NAME]
      hostAlias = host[HOST_ALIAS]

      if not hostName:
        # Find by alias in knownHosts
        hostMAC = net.getHostMAC(hostAlias)
        arp = net.getARPEntryByMAC(ARPCache, hostMAC)
        if arp and arp['known']:
          hostName = arp['ip']
          host[HOST_NAME] = hostName

          # ui.printLine('MAC: [{0}] NAME: [{1}] ALIAS: [{2}]'.format(
          #   hostMAC,
          #   hostName,
          #   hostAlias), 'MSG')

      # If not found yet it might be offline
      if not hostName:
        continue

      maxPingTime = host[HOST_MAXPING]
      stats = host[HOST_STATS]
      pingTimes = stats['pingTimes']

      # Executar ping
      result = net.ping( hostName )
      returnCode = result['returnCode']

      stats['totalCount'] += 1

      answer = result['stdout']

      # Conseguir IP
      if ut.isWindows():
        IPStr = re.search('Ping statistics for (.+?):', answer)
      else:
        # IPStr = re.search('--- (.+?) ping statistics ---', answer)
        IPStr = re.search('PING .* \((.+?)\) ', answer)

      if IPStr:
        stats['IP'] = IPStr.group(1)

      if returnCode == 0:
        # Conseguir temps del ping
        timeStr = re.search('time=(.+?)ms', answer)

        if not timeStr:
          timeStr = re.search('time<(.+?)ms', answer)

        if timeStr:
          pingTime = float(timeStr.group(1))

          # Guardar a la llista del host
          pingTimes.append(pingTime)

          # Limitar tamany de la llista de respostes
          if len(pingTimes) > pmCfg['avgPingTimeMaxSamples']:
            del pingTimes[0]

          if pingTime < maxPingTime:
            stats['okCount'] += 1
          else:
            stats['timeoutCount'] += 1

          # Calcular mitja
          stats['avgPingTime'] = reduce(lambda x, y: x + y, pingTimes) / len(pingTimes)
        else:
          # Si a la resposta del ping no aparegues 'time=(x)ms'
          pingTimes.append(-1)

          # Limitar tamany de la llista de respostes
          if len(pingTimes) > pmCfg['avgPingTimeMaxSamples']:
            del pingTimes[0]

          stats['errorCount'] += 1
      else:
        if ut.isWindows():
          timeoutStr = re.search('Request timed out', answer)
        else:
          timeoutStr = re.search('1 packets transmitted, 0 received, 100% packet loss', answer)

        if timeoutStr:
          stats['timeoutCount'] += 1
        else:
          stats['errorCount'] += 1

# Mostrar el resultat dels pings
def showResults():
  # Columna de titol
  headerStr = '{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} '.format(
    'HOST'.ljust(HOST_NAME_WIDTH),
    'IP'.ljust(HOST_IP_WIDTH),
    'ALIAS'.ljust(HOST_ALIAS_WIDTH),
    'STATE'.ljust(STATE_WIDTH),
    'PING'.ljust(LASTPING_WIDTH),
    'AVG'.ljust(AVGPING_WIDTH),
    'MAX'.ljust(MAXPING_WIDTH),
    'OK%'.ljust(PCT_WIDTH),
    'TOUT%'.ljust(PCT_WIDTH),
    'ERR%'.ljust(PCT_WIDTH),
  )

  ui.printLine(headerStr, 'HEADER')

  # Linies pels hosts
  hosts = pmCfg['hosts']

  for host in hosts:
    hostName = host[HOST_NAME]
    hostAlias = host[HOST_ALIAS]
    maxPingTime = host[HOST_MAXPING]
    stats = host[HOST_STATS]

    if host[HOST_STATS]['emptyLine']:
      hostStr = ''
    elif hostName:
      pingTimes = stats['pingTimes']
      avgPingTime = stats['avgPingTime']

      # Pot ser que algun host no hagi donat cap resposta
      if len(pingTimes) == 0:
        lastPingTime = -1;
        hostState = 'WAIT'
      else:
        lastPingTime = pingTimes[-1]

        if lastPingTime != -1:
          if lastPingTime > maxPingTime:
            if avgPingTime > maxPingTime:
              hostState = 'SLOW'
            else:
              hostState = 'TOUT'
          else:
            if avgPingTime > maxPingTime:
              hostState = 'AVG'
            else:
              hostState = 'UP'
        else:
          hostState = 'DOWN'

      # Pot ser que algun host no hagi donat cap resposta
      if lastPingTime == -1:
        lastPingTime = '? '
        avgPingTime = '? '
      else:
        lastPingTime = '{:5.2f}'.format(lastPingTime)
        avgPingTime = '{:5.2f}'.format(stats['avgPingTime'])

      maxPingTime = '{0}'.format(maxPingTime)
      totalCount = float(stats['totalCount'])
      okCount = float(stats['okCount'])
      timeoutCount =  float(stats['timeoutCount'])
      errorCount = float(stats['errorCount'])

      okPctStr = '-'
      timeoutPctStr = '-'
      errorPctStr = '-'

      if totalCount:
        if okCount:
          okPctStr = '{:6.2f}'.format(100 * (okCount / totalCount))

        if timeoutCount:
          timeoutPctStr = '{:6.2f}'.format(100 * (timeoutCount / totalCount))

        if errorCount:
          errorPctStr = '{:6.2f}'.format(100 * (errorCount / totalCount))

      hostStr = '{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} '.format(
        hostName[:HOST_NAME_WIDTH].ljust(HOST_NAME_WIDTH),
        stats['IP'][:HOST_IP_WIDTH].ljust(HOST_IP_WIDTH),
        hostAlias[:HOST_ALIAS_WIDTH].ljust(HOST_ALIAS_WIDTH),
        hostState[:STATE_WIDTH].ljust(STATE_WIDTH),
        lastPingTime[:LASTPING_WIDTH].ljust(LASTPING_WIDTH),
        avgPingTime[:AVGPING_WIDTH].ljust(AVGPING_WIDTH),
        maxPingTime[:MAXPING_WIDTH].ljust(MAXPING_WIDTH),
        okPctStr[:PCT_WIDTH].ljust(PCT_WIDTH),
        timeoutPctStr[:PCT_WIDTH].ljust(PCT_WIDTH),
        errorPctStr[:PCT_WIDTH].ljust(PCT_WIDTH),
      )

      hostStr = ui.setStrColor(hostStr, hostState)
    else:
      hostState = 'KNOWN'
      maxPingTime = '{0}'.format(maxPingTime)

      hostStr = '{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} '.format(
        '?'[:HOST_NAME_WIDTH].ljust(HOST_NAME_WIDTH),
        '?'[:HOST_IP_WIDTH].ljust(HOST_IP_WIDTH),
        hostAlias[:HOST_ALIAS_WIDTH].ljust(HOST_ALIAS_WIDTH),
        hostState[:STATE_WIDTH].ljust(STATE_WIDTH),
        '?'[:LASTPING_WIDTH].ljust(LASTPING_WIDTH),
        '?'[:AVGPING_WIDTH].ljust(AVGPING_WIDTH),
        maxPingTime[:MAXPING_WIDTH].ljust(MAXPING_WIDTH),
        '-'[:PCT_WIDTH].ljust(PCT_WIDTH),
        '-'[:PCT_WIDTH].ljust(PCT_WIDTH),
        '-'[:PCT_WIDTH].ljust(PCT_WIDTH),
      )

      hostStr = ui.setStrColor(hostStr, hostState)

    print(hostStr)

# ---[ Main code section ]--------------------------------------------

def main():
  ui.setCurrentModule('ping-monitor')
  ARPCache = net.readARPCache()

  fillConfig(ARPCache)

  while True:
    ui.cls()
    showResults()
    pingAllHosts(ARPCache)


if __name__ == '__main__':
  try:
    main()
  except:
    print()
    print()
    print('----------------------------------------------')
    print('An error happened, execution interrupted:')
    print(sys.exc_info())
